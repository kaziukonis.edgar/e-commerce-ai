export interface IHistoryItem {
  type: string
  data: string
}
