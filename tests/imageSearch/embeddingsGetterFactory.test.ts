import * as mobilenet from '@tensorflow-models/mobilenet'
import { embeddingsGetterFactory } from '../../src/imageSearch'

describe('\'embeddingsGetterFactory\' function', () => {
  it('returns embeddings getter', async () => {
    jest.setTimeout(40000)
    const model = await mobilenet.load()
    await expect(embeddingsGetterFactory(model))
      .resolves
  })
  it('gets embedding of image by url', async () => {
    jest.setTimeout(40000)
    const model = await mobilenet.load()
    const getter = await embeddingsGetterFactory(model)
    await expect(getter('https://raw.githubusercontent.com/traggo/logo/master/logo.png'))
      .resolves
  })
})
