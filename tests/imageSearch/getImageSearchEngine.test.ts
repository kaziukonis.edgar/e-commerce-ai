import {
  getImageSearchEngine,
  imageSearchFactory,
  initializeImageSearch,
} from '../../src/imageSearch'

describe('\'getImageSearchEngine\' function', () => {
  it('returns null for not defined searchEngine', async () => {
    expect(getImageSearchEngine('testImageSearch')).toBe(undefined)
  })
  it('returns searchEngine', async () => {
    jest.setTimeout(20000)
    await initializeImageSearch('./eCommerceAIData')
    const search = await imageSearchFactory('testImageSearch')
    expect(getImageSearchEngine('testImageSearch')).toBe(search)
  })
})
