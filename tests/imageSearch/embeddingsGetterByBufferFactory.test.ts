import * as mobilenet from '@tensorflow-models/mobilenet'
import { embeddingsGetterByBufferFactory } from '../../src/imageSearch'
import { fetchImage } from '../../src/utils'

describe('\'embeddingsGetterByBufferFactory\' function', () => {
  it('returns embeddingsGetterByBuffer', async () => {
    jest.setTimeout(40000)
    const model = await mobilenet.load()
    await expect(embeddingsGetterByBufferFactory(model))
      .resolves
  })
  it('embeddingsGetterByBuffer', async () => {
    jest.setTimeout(40000)
    const model = await mobilenet.load()
    const result = (await fetchImage('https://raw.githubusercontent.com/traggo/logo/master/logo.png'))
    const getter = embeddingsGetterByBufferFactory(model)
    expect(getter(await result.arrayBuffer() as Buffer))
  })
})
