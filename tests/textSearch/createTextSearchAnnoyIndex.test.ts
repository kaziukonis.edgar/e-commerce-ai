import { textSearch } from '../../src'
const { createTextSearchAnnoyIndex, initializeTextSearch } = textSearch

describe('\'createTextSearchAnnoyIndex\' function', () => {
  it('creates annoy index', async () => {
    jest.setTimeout(40000)
    await initializeTextSearch('./eCommerceAIData')
    await expect(createTextSearchAnnoyIndex([{ id: 1, text: 'test' }], 'testTextSearch'))
      .resolves
  })
})
