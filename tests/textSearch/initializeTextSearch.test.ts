import { initializeTextSearch } from '../../src/textSearch'

describe('\'initializeTextSearch\' function', () => {
  it('initializes text search', async () => {
    jest.setTimeout(20000)
    await expect(initializeTextSearch('./eCommerceAIData'))
      .resolves
  })
})
