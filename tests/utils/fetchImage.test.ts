import { fetchImage } from '../../src/utils'

describe('\'fetchImage\' function', () => {
  it('gets image', async () => {
    await expect(fetchImage('https://raw.githubusercontent.com/traggo/logo/master/logo.png'))
      .resolves
  })
  it('null url returns null', async () => {
    jest.setTimeout(80000)
    await expect(fetchImage(''))
      .resolves
  })
  it('bad url throws error', async () => {
    jest.setTimeout(80000)
    await expect(fetchImage('asd'))
      .resolves
  })
})
